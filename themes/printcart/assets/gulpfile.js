var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rtlcss = require('gulp-rtlcss');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var cssnano = require('gulp-cssnano');
var gcmq = require('gulp-group-css-media-queries');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var imagemin = require('gulp-imagemin');
var zip = require('gulp-zip');
var del = require('del');

var onError = function (err) {
    console.log('An error occurred:',err.message);
    this.emit('end');
};
gulp.task('admincss', function() {
    return gulp.src('./src/sass/admin/**/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gcmq())
        .pipe(gulp.dest('./netbase/css/admin'))
});
gulp.task('frontcss',function(){
    return gulp.src('./src/sass/**/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass())
        // .pipe(concat('main.css'))
        .pipe(autoprefixer())
        .pipe(gcmq())
        .pipe(gulp.dest('./netbase/css'))
        //.pipe(rtlcss())
        //.pipe(rename({ basename: 'rtl' }))
        //.pipe(gulp.dest('../'))

});

gulp.task('elementscss',function(){
    return gulp.src('./../../../plugins/nb-elements/assets/src/sass/frontend/**/*.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sass())
        .pipe(concat('main.css'))
        .pipe(autoprefixer())
        .pipe(gcmq())
        .pipe(gulp.dest('./../../../plugins/nb-elements/assets/css'))

});

gulp.task('frontjs',function(){
    return gulp.src('./src/js/frontend/**/*.js')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./netbase/js'))

});

gulp.task('elementsjs',function(){
    return gulp.src('./../../../plugins/nb-elements/assets/src/js/frontend/**/*.js')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./../../../plugins/nb-elements/assets/js'))

});

gulp.task('customizejs', function() {
    return gulp.src(['./src/js/admin/*.js'])
        .pipe(plumber({ errorHandler: onError }))
        .pipe(gulp.dest('./netbase/js/admin'))
});

gulp.task('watch', function() {
    gulp.watch('./src/sass/**/*.scss', gulp.parallel('frontcss','admincss'));
    gulp.watch('./src/js/**/*.js',gulp.parallel('customizejs','frontjs'));
    gulp.watch('./../../../plugins/nb-elements/assets/src/sass/**/*.scss',gulp.parallel('elementscss'));
    gulp.watch('./../../../plugins/nb-elements/assets/src/js/**/*.js',gulp.parallel('elementsjs'));
});

gulp.task('cleanAssets', function() {
    return del([
        './netbase/css/**/*',
        './netbase/js/**/*'
    ]);
});

gulp.task('buildfrontcss', function() {
    return gulp.src('./netbase/css/**/*.css')
        .pipe(cssnano({zindex:false}))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./netbase/css'));
});

gulp.task('buildfrontjs', function() {
    return gulp.src('./netbase/js/**/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./netbase/js'));
});

// gulp.task('build',gulp.series('cleanfilemin','buildfrontcss', 'buildadmincss','buildadminjs','buildfrontjs'));

gulp.task('compileAssets', gulp.parallel('frontcss', 'admincss', 'frontjs', 'customizejs'));
gulp.task('buildAssets', gulp.parallel('buildfrontcss', 'buildfrontjs'));

gulp.task('build', gulp.series('cleanAssets', 'compileAssets', 'buildAssets'));

// -- rlt

gulp.task('buildrtlcss',function(){
   return gulp.src(['./netbase/css/main.css','./netbase/css/woocommerce.css'])
       .pipe(rtlcss())
        .pipe(rename({ basename: 'rtl' }))
        .pipe(gulp.dest('../'))
});

gulp.task('backupimg', function() {
    return gulp.src('../../../uploads/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(zip('uploads.zip'))
        .pipe(gulp.dest('../../../'))
});
gulp.task('optimizeimg', function() {
    return gulp.src('../../../uploads/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(imagemin())
        .pipe(gulp.dest('../../../uploads'))
});
gulp.task('img',gulp.series('backupimg','optimizeimg'));
