<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package nbcore

 */



?>

</div><!-- #content -->

<footer id="colophon" class="site-footer confix-footer">

    <?php if(printcart_get_options('nbcore_show_footer_top')){?>
    <div class="footer-top-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="text-email">Sign me up for exclusive offers and print inspiration by email</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php echo do_shortcode("[contact-form-7 id='2580' title='email-footer']"); ?>
                </div>
                <div class="logo-wrapper-f col-md-12 text-center">
                    <?php printcart_get_footer_logo()?>
                </div>
                <?php if(!empty(printcart_get_options('nbcore_footer_title'))){ ?>
                <div class="col-md-12 text-center">
                    <div class="footer_top_title ">
                        <?php echo printcart_get_options('nbcore_footer_title');?>
                    </div>
                </div>
                <?php } ?>
                <?php if( !empty(printcart_get_options('nbcore_footer_phone')) || !empty(printcart_get_options('nbcore_footer_email')) || !empty(printcart_get_options('nbcore_footer_address'))){ ?>
                <div class="col-md-6 footer_top_left text-right">
                    <h3 class="phone"><?php echo printcart_get_options('nbcore_footer_phone');?></h3>
                    <div class="email"><a href="#"><?php echo printcart_get_options('nbcore_footer_email');?></a></div>
                    <p class="street"><?php echo printcart_get_options('nbcore_footer_address');?></p>
                </div>
                <?php } ?>
                <?php if( !empty(printcart_get_options('nbcore_footer_cap')) ){ ?>
                <div class="col-md-6 footer_top_right text-left">
                    <p class="caption"><?php echo printcart_get_options('nbcore_footer_cap'); ?></p>
                    <?php 
								if(is_active_sidebar( 'footer_newletter' )){
									dynamic_sidebar( 'footer_newletter' );
								}
								?>
                </div>
                <?php } ?>
                <div style="clear:both"></div>
                <div class="wrap-top">
                    <?php 
							if (is_active_sidebar( 'top_left' ) || is_active_sidebar( 'top_right' ) ): 

							$top_layout = printcart_get_options('nbcore_footer_top_layout');
							
							switch ($top_layout) {

								case 'layout-1':

									echo '<div class="col-lg-6 top_left">';

									dynamic_sidebar('top_left');

									echo '</div>';

									echo '<div class="col-lg-6 col-md-12 top_right">';

									dynamic_sidebar('top_right');

									echo '</div>';

									break;

								case 'layout-2':

									echo '<div class="col-lg-8 col-md-12 top_left">';

									dynamic_sidebar('top_left');

									echo '</div>';

									echo '<div class="col-lg-4 col-md-12 top_right">';

									dynamic_sidebar('top_right');

									echo '</div>';

									break;
							}
							
							endif;
							?>
                </div>
            </div>

        </div>

    </div>

    <?php }



            if( printcart_get_options('nbcore_show_footer_bot') ):?>

    <?php

			if (is_active_sidebar( 'footer-top-1' ) || is_active_sidebar( 'footer-top-2' ) || is_active_sidebar( 'footer-top-3' ) || is_active_sidebar( 'footer-top-4' ) ): ?>

    <div class="footer-bot-section ">

        <div class="container">

            <div
                class="row z-index <?php if(printcart_get_options('nbcore_footer_heading_up')){echo 'uppercase';} ?> <?php if(printcart_get_options('nbcore_footer_list_style')==false){echo 'listnone';} ?>">

                <?php

						$bot_layout = printcart_get_options('nbcore_footer_bot_layout');

						switch ($bot_layout) {

							case 'layout-1':

								echo '<div class="col-12">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								break;

							case 'layout-2':

								echo '<div class="col-6">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-6">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								break;

							case 'layout-3':

								echo '<div class="col-8">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-4">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								break;

							case 'layout-4':

								echo '<div class="col-4">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-8">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								break;

							case 'layout-5':

								echo '<div class="col-4">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-4">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								echo '<div class="col-4">';

								dynamic_sidebar('footer-top-3');

								echo '</div>';

								break;

							case 'layout-6':

								echo '<div class="col-6">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-3">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								echo '<div class="col-3">';

								dynamic_sidebar('footer-top-3');

								echo '</div>';

								break;

							case 'layout-7':

								echo '<div class="col-3">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-6">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								echo '<div class="col-3">';

								dynamic_sidebar('footer-top-3');

								echo '</div>';

								break;

							case 'layout-8':

								echo '<div class="col-3">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-3">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								echo '<div class="col-6">';

								dynamic_sidebar('footer-top-3');

								echo '</div>';

								break;

							default :

								echo '<div class="col-2 col-lg-2 col-md-2 col-sm-6 col-xs-12 col-20">';

								dynamic_sidebar('footer-top-1');

								echo '</div>';

								echo '<div class="col-2 col-lg-2 col-md-2 col-sm-6 col-xs-12 col-20">';

								dynamic_sidebar('footer-top-2');

								echo '</div>';

								echo '<div class="col-2 col-lg-2 col-md-2 col-sm-6 col-xs-12 col-20">';

								dynamic_sidebar('footer-top-3');

								echo '</div>';

								echo '<div class="col-2 col-lg-2 col-md-2 col-sm-6 col-xs-12 col-20">';

								dynamic_sidebar('footer-top-4');

								echo '</div>';

								echo '<div class="col-2 col-lg-2 col-md-2 col-sm-6 col-xs-12 col-20">';

								dynamic_sidebar('footer_bottom');

								echo '</div>';

						}

						?>

            </div>

        </div>

    </div>

    <?php endif;

			endif;?>

    <div class="footer-abs-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="box-payments">
                        <div class="text-payments">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/Icon-metro-lock.png"
                                alt="">
                            <span> SECURE PAYMENTS</span>
                        </div>
                        <div class="img-payments">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/visa-logo.png" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/american-express.png"
                                alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/maestro.png" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/mastercard.png" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/paypal.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="box-partner">
                        <div class="text-partner">
                            <span>PARTNERSHIPS & ACCREDITATIONS</span>
                        </div>
                        <div class="img-partner">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/two-sides.png" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/BPIF.png" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ipia.png" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/fsc-logo.jpg" alt="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/love-paper.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copryright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <p class="text-copyright">© 2022 TRADEPRINT. A CIMPRESS COMPANY</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <p class="text-site-maps">SITEMAP</p>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->

<?php

    if('floating' === printcart_get_options('share_buttons_position')) {

        if(printcart_get_options('nbcore_blog_single_show_social') && function_exists('nbcore_share_social')) {

            nbcore_share_social();

        }

    }

    if(printcart_get_options('show_back_top')) {

        printcart_back_to_top();

    }

    ?>

</div><!-- #site-wrapper -->

</div><!-- #page -->



<!---->

<?php wp_footer(); ?>

</body>

</html>

</body>

</html>