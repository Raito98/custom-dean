<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */ 
	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="single-product-wrap">
		<div class="product-image">
          
		<?php
			/**
			 * woocommerce_before_single_product_summary hook.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			// do_action( 'woocommerce_single_product_summary' );
			// include 'single-product/title.php';
			// include 'single-product/rating.php';
			

			do_action( 'woocommerce_before_single_product_summary' );
			do_action('woocommerce_after_shop_loop_item_title');
			do_action('add_tab_panel',$post_id);
		?>
          <?php
			$product_list = printcart_get_options('nbcore_product_list');
			 ?>

	    	<section class="related products related_products">

		        <span class="related_text"><?php esc_html_e('More products that might interest you', 'printcart'); ?></span>

		        <?php woocommerce_product_loop_start(); ?>

		        <?php
		        $args = array(
		            'post_type'      => 'product',
		            'status'         => 'publish',
		            'posts_per_page' => '6',
		        );
		        $loop = new WP_Query($args);

		        while ($loop->have_posts()) : $loop->the_post(); ?>

		            <?php
		            $post_object = get_post($loop->get_id());

		            setup_postdata($GLOBALS['post'] = &$post_object);

		            wc_get_template_part('content', 'product'); ?>
		        <?php endwhile;
		        wp_reset_query();
		        ?>
		    </section>

			<?php 

			wp_reset_postdata();
		?>
		</div>
      
		<?php 
		$nbdesigner_page_design_tool_class = '';
		$class_is_edit_mode = '';
		if(class_exists('Nbdesigner_Plugin') && is_nbdesigner_product($product->get_id())){

			$nbdesigner_page_design_tool = nbdesigner_get_option('nbdesigner_page_design_tool');
			//show design tool in new page
			if($nbdesigner_page_design_tool == 2) {
				$nbdesigner_page_design_tool_class = ' js_open_desginer_in_new_page';
			}

			if ( isset( $_REQUEST['nbo_cart_item_key'] ) && $_REQUEST['nbo_cart_item_key'] != '' ){
				$class_is_edit_mode = ' js_is_edit_mode';
			}
		}
		
		?>
		<div class="summary entry-summary<?php echo esc_attr($nbdesigner_page_design_tool_class);?><?php echo esc_attr($class_is_edit_mode);?>">
			<?php
				/**
				 * woocommerce_single_product_summary hook.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				
				do_action( 'woocommerce_single_product_summary' );
			?>
		</div><!-- .summary -->
	</div>
    <?php
    	do_action('nbd_footer_product');
    ?>


</div><!-- #product-<?php the_ID(); ?> -->
	

<?php do_action( 'woocommerce_after_single_product' ); ?>
<?php do_action( 'box_up_fix',$attr ); ?>
