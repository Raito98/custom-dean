<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :
        
		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>
        
		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
            <div class="hidden-sm hidden-xs">
            	<div class="col-md-12 mb-2 mt-1 checkout-progress-bar ">
            		<div class="arrow-steps clearfix">
            			<a href="<?php echo wc_get_cart_url(); ?>" class="step basket done current">
            				<img src="https://www.tradeprint.co.uk/.resources/tradeprint/webresources/assets/basket.png" alt="basket">
            				<span>Basket</span>
            			</a>
            			<a href="<?php echo wc_get_checkout_url(); ?>" class="step delivery-and-payment done current">
            				<span>Delivery &amp; Payment</span>
            			</a>
            			<a href="" class="step order-success">
            				<span>Order Success</span>
            			</a>
            		</div>
            	</div>
            </div>
            <style type="text/css">
    .step.current span a{
            color:#fff;
        }
        .step span a{
            color: #555;
        }
	.mb-2 {
		margin-bottom: 20px !important;
	}

	.mt-1 {
		margin-top: 10px !important;
	}

	.checkout-progress-bar .arrow-steps {
		display: flex;
		justify-content: center;
	}

	.checkout-progress-bar .arrow-steps .step.basket {
		width: 19%;
	}

	.checkout-progress-bar .arrow-steps .step.basket img {
		margin-top: 5px;
	}

	.checkout-progress-bar .arrow-steps .step span {
		position: relative;
	}

	.checkout-progress-bar .arrow-steps .step.basket.current:after,
	.checkout-progress-bar .arrow-steps .step.basket.done:after {
		right: -19px;
	}

	.checkout-progress-bar .arrow-steps .step.basket.current:after,
	.checkout-progress-bar .arrow-steps .step.delivery-and-payment.current:after{
		border-left: 19px solid #9c0;
	}
	.checkout-progress-bar .arrow-steps .step.current:after {
		border-left: 19px solid #6f6f6f;
	}
	.checkout-progress-bar .arrow-steps .step:after,
	.checkout-progress-bar .arrow-steps .step:before {
		content: " ";
		position: absolute;
		top: -1px;
		right: -19px;
		width: 0;
		border-top: 25px solid transparent;
		border-bottom: 25px solid transparent;
		border-left: 20px solid #eeecec;
		z-index: 2;
		transition: border-color .2s ease;
		height: 100%;
	}

	.checkout-progress-bar .arrow-steps .step span:before {
		opacity: 0;
		content: "✔";
		position: absolute;
		top: -2px;
		left: -20px;
	}

	.checkout-progress-bar .arrow-steps .step.upload-artwork {
		display: flex;
		flex-direction: row;
	}

	.checkout-progress-bar .arrow-steps .step.delivery-and-payment,
	.checkout-progress-bar .arrow-steps .step.order-success,
	.checkout-progress-bar .arrow-steps .step.upload-artwork {
		cursor: pointer;
		width: 27%;
	}

	.checkout-progress-bar .arrow-steps .step {
		font-size: 16px;
		font-weight: 500;
		text-align: center;
		color: #555;
		cursor: default;
		min-width: 180px;
		float: left;
		position: relative;
		background-color: #eeecec;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		transition: background-color .2s ease;
		min-height: 50px;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		border-bottom: 1px solid #d1d1d1;
		border-top: 1px solid #d1d1d1;
	}

	.checkout-progress-bar .arrow-steps .step:last-child {
		border-right: 1px solid #d1d1d1;
		border-top-right-radius: 4px;
		border-bottom-right-radius: 4px;
	}

	.checkout-progress-bar .arrow-steps .step:before {
		left: 1px;
		border-left: 20px solid #d1d1d1;
		z-index: 0;
	}

	.checkout-progress-bar .arrow-steps .step.current {
		color: #fff;
		border-color: #6f6f6f;
		background-color: #6f6f6f;
	}

	.checkout-progress-bar .arrow-steps .step:first-child {
		border-left: 1px solid #999;
		border-top-left-radius: 4px;
		border-bottom-left-radius: 4px;
	}

	.checkout-progress-bar .arrow-steps .step:first-child:before {
		border: none;
	}

	.checkout-progress-bar .arrow-steps .step span:before {
		opacity: 0;
		content: "✔";
		position: absolute;
		top: -2px;
		left: -20px;
	}

	.checkout-progress-bar .arrow-steps .step.basket {
		width: 19%;
		font-size: 14px;
		font-weight: 500;
	}
	.cart-left-section .shop_table.shop_table_responsive{
		border: 1px solid #cccc;
	}
	.shop_table.shop_table_responsive h2{
		background: #ccc;
	    height: 50px;
	    padding-top: 10px;
	    padding-left: 20px;
	}
	.woocommerce-cart-form__cart-item{
		display: flex;
	    justify-content: space-between;
	    align-items: center;
        padding-left: 15px;
    	padding-right: 15px;
    	margin-bottom: 40px;
	}
	.removes{
		border-bottom: 1px solid;		
	}
	.product-name{
	    width:70%;
	}
	.product-subtotal{
        width:20%;
	}
	.checkout-progress-bar .arrow-steps .step.done {
        background: #9c0;
        color: #fff;
        cursor: pointer;
        border-color: #9c0;
    }
</style>
            
			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>
