<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', esc_html__( 'You must be logged in to checkout.', 'printcart' ) );
	return;
}

?>
<div class="hidden-sm hidden-xs">
	<div class="col-md-12 mb-2 mt-1 checkout-progress-bar ">
		<div class="arrow-steps clearfix">
		    <a href="<?php echo wc_get_cart_url() ?>" class="step basket done current">
				<img src="https://www.tradeprint.co.uk/.resources/tradeprint/webresources/assets/basket.png" alt="basket">
				<span>Basket</span>
			</a>
			<a href="<?php echo wc_get_checkout_url() ?>" class="step delivery-and-payment current">
				<span>Delivery &amp; Payment</span>
			</a>
			<a href="" class="step order-success">
				<span>Order Success</span>
			</a>
		</div>
	</div>
</div>

<style type="text/css">
    .step.current span a{
            color:#fff;
        }
        .step span a{
            color: #555;
        }
	.mb-2 {
		margin-bottom: 20px !important;
	}

	.mt-1 {
		margin-top: 10px !important;
	}

	.checkout-progress-bar .arrow-steps {
		display: flex;
		justify-content: center;
	}

	.checkout-progress-bar .arrow-steps .step.basket {
		width: 19%;
	}

	.checkout-progress-bar .arrow-steps .step.basket img {
		margin-top: 5px;
	}

	.checkout-progress-bar .arrow-steps .step span {
		position: relative;
	}

	.checkout-progress-bar .arrow-steps .step.basket.current:after,
	.checkout-progress-bar .arrow-steps .step.basket.done:after {
		right: -19px;
	}

	.checkout-progress-bar .arrow-steps .step.basket.current:after {
		border-left: 19px solid #9c0;
	}
	.checkout-progress-bar .arrow-steps .step.current:after {
		border-left: 19px solid #6f6f6f;
	}
	.checkout-progress-bar .arrow-steps .step:after,
	.checkout-progress-bar .arrow-steps .step:before {
		content: " ";
		position: absolute;
		top: -1px;
		right: -19px;
		width: 0;
		border-top: 25px solid transparent;
		border-bottom: 25px solid transparent;
		border-left: 20px solid #eeecec;
		z-index: 2;
		transition: border-color .2s ease;
		height: 100%;
	}

	.checkout-progress-bar .arrow-steps .step span:before {
		opacity: 0;
		content: "✔";
		position: absolute;
		top: -2px;
		left: -20px;
	}

	.checkout-progress-bar .arrow-steps .step.upload-artwork {
		display: flex;
		flex-direction: row;
	}

	.checkout-progress-bar .arrow-steps .step.delivery-and-payment,
	.checkout-progress-bar .arrow-steps .step.order-success,
	.checkout-progress-bar .arrow-steps .step.upload-artwork {
		cursor: pointer;
		width: 27%;
	}

	.checkout-progress-bar .arrow-steps .step {
		font-size: 16px;
		font-weight: 500;
		text-align: center;
		color: #555;
		cursor: default;
		min-width: 180px;
		float: left;
		position: relative;
		background-color: #eeecec;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		transition: background-color .2s ease;
		min-height: 50px;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		border-bottom: 1px solid #d1d1d1;
		border-top: 1px solid #d1d1d1;
	}

	.checkout-progress-bar .arrow-steps .step:last-child {
		border-right: 1px solid #d1d1d1;
		border-top-right-radius: 4px;
		border-bottom-right-radius: 4px;
	}

	.checkout-progress-bar .arrow-steps .step:before {
		left: 1px;
		border-left: 20px solid #d1d1d1;
		z-index: 0;
	}

	.checkout-progress-bar .arrow-steps .step.current {
		color: #fff;
		border-color: #6f6f6f;
		background-color: #6f6f6f;
	}

	.checkout-progress-bar .arrow-steps .step:first-child {
		border-left: 1px solid #999;
		border-top-left-radius: 4px;
		border-bottom-left-radius: 4px;
	}

	.checkout-progress-bar .arrow-steps .step:first-child:before {
		border: none;
	}

	.checkout-progress-bar .arrow-steps .step span:before {
		opacity: 0;
		content: "✔";
		position: absolute;
		top: -2px;
		left: -20px;
	}

	.checkout-progress-bar .arrow-steps .step.basket {
		width: 19%;
		font-size: 14px;
		font-weight: 500;
	}
	.cart-left-section .shop_table.shop_table_responsive{
		border: 1px solid #cccc;
	}
	.shop_table.shop_table_responsive h2{
		background: #ccc;
	    height: 50px;
	    padding-top: 10px;
	    padding-left: 20px;
	}
	.woocommerce-cart-form__cart-item{
		display: flex;
	    justify-content: space-between;
	    align-items: center;
        padding-left: 15px;
    	padding-right: 15px;
    	margin-bottom: 40px;
	}
	.removes{
		border-bottom: 1px solid;		
	}
	.product-name{
	    width:70%;
	}
	.product-subtotal{
        width:20%;
	}
	.checkout-progress-bar .arrow-steps .step.done {
        background: #9c0;
        color: #fff;
        cursor: pointer;
        border-color: #9c0;
    }
</style>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="row">
	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col-lg-7" id="customer_details">
			<div class="checkout-billing">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="checkout-shipping">
				<?php //do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>


		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>


	<?php endif; ?>

		<div class="col-lg-5">
			<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'printcart' ); ?></h3>

			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>

			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		</div>
	</div>
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
