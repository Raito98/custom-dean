<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php esc_html_e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>
		<div class="panel-checkout null" id="checkout_login-delivery-tab">
			<div class="panel-heading">
				<h2><?php esc_html_e( 'Delivery', 'woocommerce' ); ?></h2>
			</div>
		</div>
	<?php endif; ?>
	
	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
	<div class="panel-body add-new-address">
		<div class="row">
			<div class="col-md-8 ">
				<h style="margin: 20px 0 20px;">Add New Delivery Address</h3>
			</div>
			<div class="col-md-4 text-right">
				<a href="">Select Existing Address</a>
			</div>
		</div>
		<div class="row">
			<label for="postcode" class="col-sm-4 col-xs-12">Postcode*</label>
			<div class="col-sm-8 col-xs-12">
				<?php if ( apply_filters( 'woocommerce_shipping_calculator_enable_postcode', true ) ) : ?>
				<p class="form-row address-field form-row-wide"  id="calc_shipping_postcode_field">
					<input type="text" class="input-text" value="<?php echo esc_attr( WC()->customer->get_shipping_postcode() ); ?>" placeholder="<?php esc_attr_e( 'Postcode / ZIP', 'woocommerce' ); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
				</p>
			<?php endif; ?>
				<div class="col-sm-8 col-xs-12 checkbox_input">
					<input type="checkbox" class="customCheckbox" name="enterAddressManually" id="enterAddressManually" >
					<label for="enterAddressManually" class="light">Enter Address Manually</label>
				</div>
			</div>

		</div>
	</div>
	<div class="woocommerce-billing-fields__field-wrapper">
		<div>
			<?php
			$fields = $checkout->get_checkout_fields( 'billing' );

			foreach ( $fields as $key => $field ) {
				woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			}
			?>
		</div>
		<div class="form-group">
				<div class="checkbox_input">
					<input type="checkbox" class="customCheckbox" name="saveBillingAddress" id="saveBillingAddress">
					<label for="saveBillingAddress" class="light">Save as Billing Address</label>
				</div>
				<div class="checkbox_input">
					<input type="checkbox" class="customCheckbox" name="saveBillingAddress" id="saveBillingAddress">
					<label for="saveBillingAddress" class="light">Save as Billing Address</label>
				</div>
				<div class="button-delevery" id="button-delevery">
					<a class="btn btn-block btn-orange btn-chkout">ADD</a>
				</div>
		</div>
	</div>
	<div class="panel panel-checkout null" id="checkout_login-billing-tab">
		<div class="panel-heading">
			<h2>BILLING ADDRESS</h2>
		</div>
	</div>
	<div class="woocommerce-billing-adress__field-wrapper">
		<div>
			<?php
			$fields = $checkout->get_checkout_fields( 'billing' );

			foreach ( $fields as $key => $field ) {
				woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			}
			?>
		</div>
		<div class="form-group">
				<div class="checkbox_input">
					<input type="checkbox" class="customCheckbox" name="saveBillingAddress" id="saveBillingAddress">
					<label for="saveBillingAddress" class="light">Save Billing Address</label>
				</div>
				<div class="button-delevery" id="button-adress">
					<a class="btn btn-block btn-orange btn-chkout">NEXT</a>
				</div>
		</div>
	</div>
	<div class="panel panel-checkout null" id="checkout_login-payment-tab">
		<div class="panel-heading">
			<h2>PAYMENT METHOD</h2>
		</div>
	</div>
	<div class="payment-checkout">
		<?php
		/**
		 * Terms and conditions hook used to inject content.
		 *
		 * @since 3.4.0.
		 * @hooked wc_checkout_privacy_policy_text() Shows custom privacy policy text. Priority 20.
		 * @hooked wc_terms_and_conditions_page_content() Shows t&c page content. Priority 30.
		 */
		do_action( 'woocommerce_checkout_terms_and_conditions' );
		?>
	</div>
	<script type="text/javascript">
		jQuery("#enterAddressManually").click(function(){
			if(jQuery("#enterAddressManually").is(':checked'))
				{
				  jQuery(".woocommerce-billing-fields__field-wrapper").css("display","block");
				  jQuery(".woocommerce-billing-fields__field-wrapper").css("margin-bottom","20px");
				  
				}
			else {
					jQuery(".woocommerce-billing-fields__field-wrapper").css("display","none");
					
				}
			});
		  jQuery("#button-delevery").click(function(){
		  	jQuery(".woocommerce-billing-adress__field-wrapper").css("display","block");
		  });
		  jQuery("#button-adress").click(function(){
		  	jQuery(".payment-checkout").css("display","block");
		  });

		
	</script>
	<style type="text/css">
		.panel{
			margin-bottom: 20px;
		}
		.panel-body{
			margin-bottom: 20px;
		}
		.panel-heading{
			background-color: #5c5c5c;
		    border: 1px solid #5c5c5c;
		    color: #fff;
		    border-radius: 2px 2px 0 0;
		    padding: 10px 30px;
		}
		.panel-heading h2{
		    font-size: 1.2em;
		    font-weight: 500;
		    margin: 0;
		    display: inline-block;
	    	float: none;
	    	color: #fff;

		}
		.panel-body{
			background-color: #efeded;
		    border: 1px solid #efeded;
		    border-radius: 0 0 2px 2px;
		    padding: 5px 30px;
		}
		.woocommerce-billing-fields__field-wrapper{
			display: none;
		}
		input.customCheckbox:checked, input.customCheckbox:focus {
		    outline: 0;
		}
		input.customCheckbox:checked:after {
		    content: "✓";
		    position: absolute;
		    font-weight: 900;
		    color: #007cc0;
		}
		input.customCheckbox {
			appearance: none;
		    width: 22px;
		    height: 22px;
		    border: 1px solid #ccc;
		    border-radius: 2px;
		    outline: 0;
		    margin: 0 0 -5px;
		}
		p.validate-postcode{
			display: none;

		}
		.checkbox_input{
			margin-bottom: 5px;
		}
		.button-delevery a:hover{
			color: #fff;
		}
		.woocommerce-billing-adress__field-wrapper{
			display: none;
		}
		.payment-checkout{
			display: none;
		}
	</style>
	

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>

<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
	<div class="woocommerce-account-fields">
		<?php if ( ! $checkout->is_registration_required() ) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ); ?> type="checkbox" name="createaccount" value="1" /> <span><?php esc_html_e( 'Create an account?', 'woocommerce' ); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

			<div class="create-account">
				<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
	</div>
<?php endif; ?>
