<div class="hidden-sm hidden-xs">
	<div class="col-md-12 mb-2 mt-1 checkout-progress-bar ">
		<div class="arrow-steps clearfix">
			<a href="<?php echo wc_get_cart_url() ?>" class="step basket current">
				<img src="https://www.tradeprint.co.uk/.resources/tradeprint/webresources/assets/basket.png" alt="basket">
				<span>Basket</span>
			</a>
			<a href="<?php echo wc_get_checkout_url() ?>" class="step delivery-and-payment">
				<span>Delivery &amp; Payment</span>
			</a>
			<a href="" class="step order-success">
				<span>Order Success</span>
			</a>
		</div>
	</div>
</div>

<style type="text/css">
    .step.current span a{
        color:#fff;
    }
    .step span a{
        color: #555;
    }
	.mb-2 {
		margin-bottom: 20px !important;
	}

	.mt-1 {
		margin-top: 10px !important;
	}

	.checkout-progress-bar .arrow-steps {
		display: flex;
		justify-content: center;
	}

	.checkout-progress-bar .arrow-steps .step.basket {
		width: 19%;
	}

	.checkout-progress-bar .arrow-steps .step.basket img {
		margin-top: 5px;
	}

	.checkout-progress-bar .arrow-steps .step span {
		position: relative;
	}

	.checkout-progress-bar .arrow-steps .step.basket.current:after,
	.checkout-progress-bar .arrow-steps .step.basket.done:after {
		right: -19px;
	}

	.checkout-progress-bar .arrow-steps .step.current:after {
		border-left: 19px solid #6f6f6f;
	}

	.checkout-progress-bar .arrow-steps .step:after,
	.checkout-progress-bar .arrow-steps .step:before {
		content: " ";
		position: absolute;
		top: -1px;
		right: -19px;
		width: 0;
		border-top: 25px solid transparent;
		border-bottom: 25px solid transparent;
		border-left: 20px solid #eeecec;
		z-index: 2;
		transition: border-color .2s ease;
		height: 100%;
	}

	.checkout-progress-bar .arrow-steps .step span:before {
		opacity: 0;
		content: "✔";
		position: absolute;
		top: -2px;
		left: -20px;
	}

	.checkout-progress-bar .arrow-steps .step.upload-artwork {
		display: flex;
		flex-direction: row;
	}

	.checkout-progress-bar .arrow-steps .step.delivery-and-payment,
	.checkout-progress-bar .arrow-steps .step.order-success,
	.checkout-progress-bar .arrow-steps .step.upload-artwork {
		cursor: pointer;
		width: 27%;
	}

	.checkout-progress-bar .arrow-steps .step{
		font-size: 16px;
		font-weight: 500;
		text-align: center;
		color: #555;
		cursor: default;
		min-width: 180px;
		float: left;
		position: relative;
		background-color: #eeecec;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		transition: background-color .2s ease;
		min-height: 50px;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		border-bottom: 1px solid #d1d1d1;
		border-top: 1px solid #d1d1d1;
	}

	.checkout-progress-bar .arrow-steps .step:last-child {
		border-right: 1px solid #d1d1d1;
		border-top-right-radius: 4px;
		border-bottom-right-radius: 4px;
	}

	.checkout-progress-bar .arrow-steps .step:before {
		left: 1px;
		border-left: 20px solid #d1d1d1;
		z-index: 0;
	}

	.checkout-progress-bar .arrow-steps .step.current {
		color: #fff;
		border-color: #6f6f6f;
		background-color: #6f6f6f;
	}

	.checkout-progress-bar .arrow-steps .step:first-child {
		border-left: 1px solid #999;
		border-top-left-radius: 4px;
		border-bottom-left-radius: 4px;
	}

	.checkout-progress-bar .arrow-steps .step:first-child:before {
		border: none;
	}

	.checkout-progress-bar .arrow-steps .step span:before {
		opacity: 0;
		content: "✔";
		position: absolute;
		top: -2px;
		left: -20px;
	}

	.checkout-progress-bar .arrow-steps .step.basket {
		width: 19%;
		font-size: 14px;
		font-weight: 500;
	}
	.cart-left-section .shop_table.shop_table_responsive{
		border: 1px solid #cccc;
	}
	.shop_table.shop_table_responsive h2{
		background: #ccc;
	    height: 50px;
	    padding-top: 10px;
	    padding-left: 20px;
	}
	.woocommerce-cart-form__cart-item{
		display: flex;
	    justify-content: space-between;
	    align-items: center;
        padding-left: 15px;
    	padding-right: 15px;
    	margin-bottom: 40px;
	}
	.removes{
		border-bottom: 1px solid;		
	}
	.product-name{
	    width:70%;
	}
	.product-subtotal{
        width:20%;
	}
</style>
<div class="row">
	<div class="cart-left-section col-lg-8">
		<?php do_action('woocommerce_before_cart_table'); ?>
		<div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
			<h2>YOUR BASKET</h2>
			
				<?php do_action('woocommerce_before_cart_contents'); ?>

				<?php
				foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
					$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
					$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

					if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
						$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
				?>
						<div class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">

							

							<div class="product-name" data-title="<?php esc_attr_e('Product', 'printcart'); ?>">
								<?php
									$_product =  wc_get_product($cart_item['data']->get_id());
								?>
								
									<?php
									echo "<h3>" . $_product->get_title() . '</h3>  Quantity: ' . $cart_item['quantity'] . '&nbsp;';

									if($cart_item['nbo_meta']['option_price']['fields']){
											foreach ($cart_item['nbo_meta']['option_price']['fields'] as $key => $value) {
										
										
											echo $value['name'] . ':' . $value['value_name'] . '&nbsp;';
										

											}
										}
									?>
								
							</div>
							<div class="product-subtotal" data-title="<?php esc_attr_e('Total', 'printcart'); ?>">
								<div>
								<?php
								
								echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);
								echo '<p class="product_vat"></p>';
								echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="removes" aria-label="%s" data-product_id="%s" data-product_sku="%s">Remove</i></a>',
									esc_url(wc_get_cart_remove_url($cart_item_key)),
									__('Remove this item', 'printcart'),
									esc_attr($product_id),
									esc_attr($_product->get_sku())
								), $cart_item_key);
								
								?>
							</div>
							</div>

						</div>
						<?php
							}
						}
						?>

						<?php do_action('woocommerce_cart_contents'); ?>

						

						<?php do_action('woocommerce_after_cart_contents'); ?>
				
		</div>
		<?php do_action('woocommerce_after_cart_table'); ?>
	</div>

	<div class="cart-right-section col-lg-4">
		<?php
		woocommerce_cart_totals();
		$button_class = '';
		if ('cart-layout-1' === printcart_get_options('nbcore_cart_layout')) {
			$button_class = 'nb-primary-button';
		} else {
			$button_class = 'nb-wide-button button';
		}
		if (wc_coupons_enabled()) { ?>
			<div class="coupon">
				<h3>Coupon</h3>
				<div class="coupon-wrap">
					<input type="text" name="coupon_code" class="input-text bt-5" id="coupon_code" value="" placeholder="<?php esc_attr_e('Coupon code', 'printcart'); ?>" />
					<input type="submit" class="bt-5 <?php echo esc_attr($button_class); ?>" name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'printcart'); ?>" />
					<?php do_action('woocommerce_cart_coupon'); ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<div class="cart-collaterals">
	<?php
	if (printcart_get_options('nbcore_show_cross_sells')) {
		woocommerce_cross_sell_display();
	} ?>
</div>