<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<style type="text/css">
	.btn-cnt-shp.basket-page {
	    background-color: #fff;
	    color: #007cc0;
	    font-size: 16px;
	    padding: 10px;
	    font-weight: 500;
	    display: block!important;
	    text-decoration: revert;
	}
	.btn-cnt-shp.basket-page {
	    background-color: #fff;
	    color: #007cc0;
	    font-size: 16px;
	}
	.panel-cart {
	    border-radius: 2px;
	    border: none;
	    box-shadow: none;
	}
	.panel-summary-checkout {
	    color: #717171;
	}
	.panel {
	    margin-bottom: 20px;
	    background-color: #fff;
	    border: 1px solid transparent;
	    border-radius: 4px;
	    -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
	    box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
	}
	.panel-heading {
	    padding: 10px 15px;
	    border-bottom: 1px solid transparent;
	    border-top-left-radius: 3px;
	    border-top-right-radius: 3px;
	}
	.panel-cart .panel-heading h1, .panel-cart .panel-heading h2 {
	    font-size: 1.2em;
	    font-weight: 500;
	    margin: 0;
	    color: #333;
	}
	.inline-block {
	    display: inline-block;
	    float: none;
	}
	.summary_row {
	    margin-top: 10px;
	    margin-bottom: 10px;
	}
	.col-xs-6 {
	    width: 50%;
	    float: left;
        position: relative;
		min-height: 1px;
		padding-right: 15px;
		padding-left: 15px;
	}
	.text-right {
	    text-align: right;
	}
	.btn-orange, .btn-orange:focus {
	    color: #fff;
	}
	.btn-chkout {
	    padding: 10px!important;
	}
	.btn.disabled, .btn[disabled], fieldset[disabled] .btn {
	    box-shadow: none;
	}
	.btn-orange:hover {
	    color: #f3f3f3;
	}
	.panel-cart .panel-heading {
	    background-color: #efeded;
	    border: 1px solid #efeded;
	    border-radius: 2px;
	    padding: 10px 30px;
	}
	.panel-cart .panel-heading h2 {
	    font-size: 1.2em;
	    font-weight: 500;
	    margin: 0;
	    color: #333;
	}
	.panel-cart .panel-body {
	    border: 1px solid #efeded;
	    border-radius: 2px;
	    padding: 30px;
	}

</style>
	<a href="https://www.tradeprint.co.uk" class="text-center d-block btn-cnt-shp mb-2 basket-page">CONTINUE SHOPPING</a>
	<div class="panel panel-cart panel-summary-checkout">
		<div class="panel-heading">
			<h2 class="inline-block">Tradeprint Vouchers</h2>
		</div>
		<div>
			<div class="row summary_row">
				<div class="col-xs-6">Total Reward Available</div>
				<div class="col-xs-6 text-right">
					<strong id="totalNetCentAmount">£0.00</strong>
				</div>
			</div>
			<div class="row summary_row">
				
					<div class="col-xs-6">Reward Available For This Order</div>
					<div class="col-xs-6 text-right">
						<strong id="totalTaxCentAmount">£0.00</strong>
					</div> 
				
			</div>
			<button class="btn btn-block btn-orange btn-chkout" disabled="">REDEEM</button>
		</div>
		<div class="panel-heading">
			<h2 class="inline-block">SUMMARY</h2>
		</div><div class="panel-body">
			<div class="row summary_row">
				<div class="col-xs-6">SUB TOTAL</div>
				<div class="col-xs-6 text-right">
					<strong class="woocommerce-Price-amount amount"><?php wc_cart_totals_subtotal_html(); ?></strong>
				</div>
			</div>
			<div class="row summary_row">
				<div class="col-xs-6">VAT</div><div class="col-xs-6 text-right">
					<strong class="vat"></strong>
				</div>
			</div>
			<div class="row summary_row">
			<div class="col-xs-6 sum_total">TOTAL</div>
			<div class="col-xs-6 text-right">
				<h4 class="price_total"></h4>
			</div></div><hr class="summary_hr">
			<div><?php do_action( 'woocommerce_proceed_to_checkout' ); ?></div>
		</div>
	</div>

<!-- <div class="cart_totals <?php if ( WC()->customer->has_calculated_shipping() ) echo 'calculated_shipping'; ?>">

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>

	<h3><?php esc_html_e( 'Cart totals', 'printcart' ); ?></h3>

	<div class="cart-totals-wrap">
		<table cellspacing="0" class="shop_table shop_table_responsive">

			<tr class="cart-subtotal">
				<th><?php esc_html_e( 'Subtotal', 'printcart' ); ?></th>

				<td data-title="<?php esc_attr_e( 'Subtotal', 'printcart' ); ?>"><?php wc_cart_totals_subtotal_html(); ?></td>

			</tr>
			<tr class="cart-subtotal">
				<th><?php esc_html_e( 'VAT', 'printcart' ); ?></th>

				<td class="vat"> data-title="<?php esc_attr_e( 'VAT', 'printcart' ); ?>"></td>
			
			</tr>

			<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
				<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
					<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
					<td data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>"><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
				</tr>
			<?php endforeach; ?>

			<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

				<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>

				<?php wc_cart_totals_shipping_html(); ?>

				<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>

			<?php elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>

				<tr class="shipping">
					<th><?php esc_html_e( 'Shipping', 'printcart' ); ?></th>
					<td data-title="<?php esc_attr_e( 'Shipping', 'printcart' ); ?>"><?php woocommerce_shipping_calculator(); ?></td>
				</tr>

			<?php endif; ?>

			<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
				<tr class="fee">
					<th><?php echo esc_html( $fee->name ); ?></th>
					<td data-title="<?php echo esc_attr( $fee->name ); ?>"><?php wc_cart_totals_fee_html( $fee ); ?></td>
				</tr>
			<?php endforeach; ?>

			<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) :
				$taxable_address = WC()->customer->get_taxable_address();
				$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
					? sprintf( ' <small>' . esc_html__( '(estimated for %s)', 'printcart' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )
					: '';

				if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
					<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
						<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
							<th><?php echo esc_html( $tax->label ) . $estimated_text; ?></th>
							<td data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else : ?>
					<tr class="tax-total">
						<th><?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; ?></th>
						<td data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>"><?php wc_cart_totals_taxes_total_html(); ?></td>
					</tr>
				<?php endif; ?>
			<?php endif; ?>

			<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

			<tr class="order-total">
				<th><?php esc_html_e( 'Total', 'printcart' ); ?></th>
				<td data-title="<?php esc_attr_e( 'Total', 'printcart' ); ?>"><p class="price_total"></p></td>
			</tr>

			<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>

		</table>

		<div class="wc-proceed-to-checkout">
			<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
		</div>
	</div>

	<?php do_action( 'woocommerce_after_cart_totals' ); ?>

</div> -->
