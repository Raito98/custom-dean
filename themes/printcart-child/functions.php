<?php

function homeone_print_embed_style($css)
{
	$primary_color = printcart_get_options('nbcore_primary_color');
	$text_link = printcart_get_options('nbcore_link_hover_color');
	$text_link_hover = printcart_get_options('nbcore_link_color');
	$css .= "
	.site-footer .footer-bot-section .widget ul li a:before {
		top: 2px;
	}
	.main-navigation .menu-main-menu-wrap #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link {
		font-weight: 600;
		font-family: Poppins;
	}
	.ib-shape .icon{                
		background-color: " . esc_attr($primary_color) . ";
	}
	.vc-blog .blog-content .hb-readmore {
		color: ". esc_attr($text_link) .";
	}
	.vc-blog .blog-content .hb-readmore:hover {
		color: ". esc_attr($text_link_hover) .";
	}
	footer.site-footer .footer-top-section .footer_top_title, .site-footer .footer-bot-section .widget ul li a, footer.site-footer p {
		color: #ccc;
	}
	.woocommerce form .form-row abbr.required {
		text-decoration: none;
		border: none;
	}
	.nbtcs-select {
		top: 27px;
	}
	.nbtcs-w-wrapper .selected {
		padding-left: 3px;
	}
	.vc-blog .blog-content .hb-readmore {
		color: #666;
	}
	.ib-shape .icon i {
		line-height: 65px;
	}
	.comments-area {
		background-color: #fff;
	}
	.nbtcs-w-wrapper .selected {
    		padding-left: 5px;
	}
	.nbtcs-w-wrapper .selected:after {
		margin: 0 0 0 5px;
	}
	@media (min-width: 640px) and (max-width: 768px) {
		.shop_table.cart .actions input.bt-5 {
			float: right;
		}
	}
	@media only screen and (max-width: 768px) {
		body > div#page {
			overflow: hidden;
		}
		.shop_table.cart .cart_item td.product-price, .shop_table.cart .cart_item td.product-quantity {
			padding-right: 30px;
		}
	}
	@media only screen and (max-width: 320px) {
		.middle-section-wrap .middle-right-content .minicart-header .mini-cart-wrap {
			right: -38px !important;
		}
		.header-1 .middle-section-wrap .middle-right-content .header-cart-wrap {
			margin-left: 19px;
		}
		.shop_table.cart td:before {
			position: absolute !important;
			left: 15px !important;
		}
		.shop_table.cart .product-remove {
			text-align: center;
		}
		.shop_table.cart .actions input.bt-5 {
			width: 95%;
		}
	}
	";
	return $css;
}

add_filter('printcart_css_inline', 'homeone_print_embed_style');
add_action( 'after_setup_theme', 'wc_remove_frame_options_header', 11 );
function wc_remove_frame_options_header() {		
	remove_action( 'template_redirect', 'wc_send_frame_options_header' );	
};

add_action('wp_enqueue_scripts', 'add_font');
function add_font()
{
		
?>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<?php
}
add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts');
function wpdocs_theme_name_scripts()
{
	wp_register_script ( 'custom-script', get_theme_file_uri() . '/js_custorm/js.js' );
	wp_enqueue_script ( 'custom-script' );
	
?>
<?php
}

// add shortcode review.io 
function create_shortcode() { 
	?>
<script src="https://widget.reviews.io/badge-ribbon/dist.js"></script>
<div id="badge-ribbon"></div>
<script>
reviewsBadgeRibbon("badge-ribbon", {
    store: "printing-next-day",
    size: "medium",


});
</script>
<?php
}
add_shortcode('reivew_shortcode','create_shortcode');
// end add shortcode review.io 

// overwrite theme
add_action('woocommerce_before_shop_loop', 'woocommerce_term_title', 20);
function woocommerce_term_title() {
	global $wp_query;
	$cat = $wp_query->get_queried_object();
	if( is_shop() ){
		echo '<h1>' . esc_attr( $cat->label ) .'</h1>'; 
	}else{ 
		echo '<h1 class="overwrite-title">' . esc_attr( $cat->name ) .'</h1>';
		echo category_description($cat->term_id);
		echo do_shortcode( '[reivew_shortcode]' ); 
	}
}
// end overwrite theme

add_action( 'wp_enqueue_scripts', 'home_styles', 99);
function home_styles() {
    //wp_enqueue_style( 'fontello', get_stylesheet_directory_uri() . '/assets/css/fontello.css' );
    wp_enqueue_style( 'custom-home', get_stylesheet_directory_uri() . '/assets/css/custom-home.css' ); 
     wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/assets/css/style.css' ); 
      //wp_enqueue_style( 'product_detail', get_stylesheet_directory_uri() . '/assets/css/product_detail.css' ); 
     if( is_product() ){
         wp_enqueue_style( 'product_detail', get_stylesheet_directory_uri() . '/assets/css/product_detail.css' ); 
     }
}
 

add_filter('wp_nav_menu_objects','printcart_nav_menu_objects',20,2);
function printcart_nav_menu_objects($sorted_menu_items, $args){
    return $sorted_menu_items;
}



function get_data_sql($value)
{
	global $wpdb;
	$table_name=$wpdb->prefix . 'posts';
	$sql="SELECT ID,post_title FROM $table_name WHERE post_type = 'product' and (post_title like '$value%') ";
	$resu = $wpdb->get_results($sql);
	
	return $resu;
}
add_filter('megamenu_walker_nav_menu_start_el','printcart_walker_nav_menu_start_el',50,4);
function printcart_walker_nav_menu_start_el($item_output, $item, $depth, $args){
	global $wpdb, $product;  
	if($item->object != 'product_cat') return $item_output;
    $menu_items = wp_get_nav_menu_items( $args->menu->term_id, array( 'update_post_term_cache' => false ) );
    $taxonomies = array( 
        'taxonomy' => 'product_cat'
    );
    $args = array(
        'taxonomy' => 'product_cat',
        'parent' =>   $item->object_id,
        'hide_empty' => false,
    ); 
    $terms = get_terms($args);
    $amount = count($terms);
    // echo '<pre>';
	// print_r($result);
	// echo "</pre>";
	// die();
    ob_start();
    
    if($item->title=='All product'){
    	?>
    	<ul class="dropdown-menu nav_main-top-level indexed mega-menu-4-column ">
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hidden-xs hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="A">A</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('a');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link  hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="B">B</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('b');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link  hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="C">C</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('c');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link  hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="D">D</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('d');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link  hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="E">E</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('e');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="F">F</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('f');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="G">G</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('g');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="H">H</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('h');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="I">I</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('i');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="j">J</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('j');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="k">K</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('k');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="L">L</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('l');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="M">M</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('m');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="N">N</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('n');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="O">O</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('o');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="P">P</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('p');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="Q">Q</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('q');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="R">R</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('r');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="S">S</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('s');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="T">T</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('t');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="U">U</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('u');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="V">V</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('v');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="W">W</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('w');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="X">X</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('x');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="Y">Y</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('y');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    		<div class="hidden-xs w-20">
    			<li class="nav_main-2-link hovered" >
    				<span class="mega-sub-cat-heading has-subMenues mn_main" sub-category="Z">Z</span>
    				<ul class="nav_main-level-3">
    						<?php
			    				$test = get_data_sql('z');
								foreach ($test as $key => $value) {
									$url = get_permalink( $value->ID ) ;
			    			?>
			    			
			    				<li class="nav_sub-3-link" third-level="1">
			    					<a href="<?php echo $url ?>"><?php echo $value->post_title ?></a>
			    				</li>
			    			

			    			<?php } ?>
    					
    				</ul>
    			</li>
    		</div>
    
    	</ul>


    	<?php

	}
    else{
    	?>
    		<ul class="nav_sub_menu_category">
                
            <div class="menu-content <?php echo esc_attr($amount > 2 ? 'main-menu-style-2' : 'main-menu-style-1') ?>">
                <div class="menu-content-top menu-content-sub-category">
                    <?php foreach($terms as $term) : 
                        $subthumb_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                        if($subthumb_id){
                            $src_sub = wp_get_attachment_image_url($subthumb_id);
                        }else{
                            $src_sub = wc_placeholder_img_src();
                        }
                        $link = get_term_link( $term->term_id, 'product_cat' );
                        $args = array(
                            'post_type'             => 'product',
                            'post_status'           => 'publish',
                            'posts_per_page'        => 10,
                            'tax_query'             => array(
                                array(
                                    'taxonomy'      => 'product_cat',
                                    'field'         => 'term_id',
                                    'terms'         => $term->term_id,
                                    'operator'      => 'IN'
                                ),
                                array(
                                    'taxonomy'      => 'product_visibility',
                                    'field'         => 'slug',
                                    'terms'         => 'exclude-from-catalog',
                                    'operator'      => 'NOT IN'
                                )
                            )
                        );
                        $loop = new WP_Query( $args );
                        
                        ?>
                    <ul class="menu-sub-category-item productItem <?php //echo esc_attr($amount > 2 ? 'col4' : 'col2') ?>">
                        
                       
                            <li class="sub-category-menu">
                                <a href="#" class="sub-category-title-link"><h3 class="sub-category-title"><?php esc_html_e($term->name) ?></h3></a>
                                <ul class="menu-sub-category-products-parent">
                                    <?php
                                        while ( $loop->have_posts() ) : $loop->the_post();
                                            global $product;
    										$src = wp_get_attachment_image_url( get_post_thumbnail_id( $product->get_id() ), 'single-post-thumbnail' );
    										if(!$src)  $src = wc_placeholder_img_src();
                                            ?>
                                            <li class="menu-sub-category-product" data-src="<?php echo $src ?>">
                                                <a href="<?php echo esc_url(get_permalink()) ?>" target="_blank" class="product-name"><?php esc_html_e(get_the_title()) ?>
                                                </a>
                                            </li>
                                            <?php
                                        endwhile;
                                        wp_reset_query();
                                    ?>
                                </ul>
                            </li>
                        
                    </ul>
                    
                    <?php endforeach; ?>
                </div>  
                 
            </div>  
        </ul>
       
        <script>
            jQuery(".menu-sub-category-item.productItem > .sub-category-menu").hover(function(e){
                this.querySelector(".menu-sub-category-products-parent").classList.add("show");
                // jQuery(".nav_sub_menu_category").clientWidth =this.querySelector(".menu-sub-category-products-parent.show").clientHeight;
            }, function() {
                this.querySelector(".menu-sub-category-products-parent").classList.remove("show");
            });
        </script>
    	<?php

    }
    
    $con = ob_get_clean();

    return $item_output . $con;
}



