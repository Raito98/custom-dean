<?php
/**
 * Plugin Name:       Grid view delivery table for NBDesigner Advanced
 * Plugin URI:        https://cmsmart.net
 * Description:       Grid on/off for delivery table
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.1
 * Author:            Hoang
 * Author URI:        https://cmsmart.net
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       grid-view-delivery-table
 * Domain Path:       /languages
 */


add_action('wp_enqueue_scripts','nbod_custom_grid_nbdesigner_js',31);
function nbod_custom_grid_nbdesigner_js(){
    wp_localize_script('nbdesigner','nbod_gridview',true);
    wp_register_style( 'grid-view', plugin_dir_url( __FILE__ ) . 'gridview.css', array(), '1.0.0' );
    wp_enqueue_style( 'grid-view' );
    wp_register_script( 'grid-view', plugin_dir_url( __FILE__ ) . 'gridview.js', array( 'jquery' ), '1.0.0' );
    wp_enqueue_script( 'grid-view' );
}
add_action('admin_enqueue_scripts','nbod_admin_custom_grid_nbdesigner_js',31);
function nbod_admin_custom_grid_nbdesigner_js(){
    wp_localize_script('admin_nbdesigner','nbod_gridview',true);  
}

add_action('nbd_js_config','nbod_custom_grid_frontend_js');
function nbod_custom_grid_frontend_js(){
    echo 'var nbod_gridview = true;';
}

add_filter('nbd_change_delivery_section_layout','change_delivery_section_layout',10,4);
function change_delivery_section_layout($html,$delivery_field,$request,$nbd_qv_type){
    ob_start();
    ?>
    <div class="nbo-delivery" ng-if="valid_form && nbd_fields['<?php echo $delivery_field['id']; ?>'].enable">
        <div class="section-header">
            <p><?php _e('Pricing Table (On/Off)', 'web-to-print-online-designer'); ?></p>
            <div class="toogle-btn">
                <label class="switch">
                  <input type="checkbox" name="change_layout" checked>
                  <span class="slider round"></span>
                </label>
            </div>
            
        </div>
      <div class="position-relative bg-gray-canny py-1 px-1">
        <p class="mb-0 f-14 txt-bold text-dark">Can’t find the price your looking for? <a href="#" class="d-inline-block mx-1 text-underline txt-heavy" title="Submit request for something else">Request A Quote</a>
        </p>
      </div>
        <div class="nbo-delivery-wrapper" <?php if( isset($request['wc-api']) && $request['wc-api'] == 'NBO_Quick_View' && $nbd_qv_type == '2') echo 'nbd-perfect-scroll'; ?>>
            <table>
                <thead>
                 
                    <tr>
                       <th class="nbo-delivery-icon-wrap custom_nb" >
                        
                        <div class="nbo-delivery-qty"><?php _e('Quantity', 'web-to-print-online-designer'); ?></div>
                    </th>
                        <?php 
                            foreach ( $delivery_field['general']['attributes']["options"] as $key => $attr ): 
                                $delivery_days  = absint( $attr['delivery'] );
                                $delivery_date  = date('Y-m-d', strtotime("+{$delivery_days} weekdays"));
                                $date           = date_i18n( 'l', strtotime( "$delivery_date" ) );
                                $day            = date_i18n( 'j', strtotime( "$delivery_date" ) );
                                $month          = date_i18n( 'M', strtotime( "$delivery_date" ) );
                        ?>
                      <th>
                            <div class="nbo-delivery-date-wrap">
                                <div class="nbo-delivery-date-inner">
                                    <div class="nbo-delivery-date-title"><?php echo $attr['name']; ?></div>
                                    
                                    <div class="nbo-delivery-date2"><span class="day"><?php echo $day; ?></span> <?php echo $month; ?></div>
                                </div>
                            </div>
                        </th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="(bIndex, _break) in turnaround_quantity_breaks">
                        <th>{{_break.val}}</th>
                        <td ng-repeat="(tIndex, t) in turnaround_matrix[bIndex]" ng-class="{'active': t.active, 'nbdd_disable': !t.show}" class="nbo-delivery-date-selector" ng-click="change_delivery_date( bIndex, tIndex, $event );">
                            <span ng-if="t.show" class="nbo-delivery-total" ng-bind-html="t.total_cart_price | to_trusted"></span>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="nbo-delivery-short-wrap" <?php if( isset($request['wc-api']) && $request['wc-api'] == 'NBO_Quick_View' && $nbd_qv_type == '2') echo 'nbd-perfect-scroll'; ?>>
            <div class="quantity-selection nbd-option-field">
                <div class="nbd-field-header"><label>Quantity</label></div>
                <div class="nbd-field-content">
                    <select class="qty-field nbo-dropdown" name="qty_op" id="selectedQty" ng-model="customQty" ng-change="update_custom_qty()">
                        <option ng-repeat="(bIndex, _break) in turnaround_quantity_breaks track by $index" value="{{_break.val}}">{{_break.val}}</option>
                    </select>
                </div>  
            </div>  
            <div class="delivery-selection">
                <table>
                    <tbody>
                        
                        <tr>
                  
                            <?php 
                                foreach ( $delivery_field['general']['attributes']["options"] as $key => $attr ): 
                                    // $delivery_days  = absint( $attr['delivery'] );
                                    // $delivery_date  = date('Y-m-d', strtotime("+{$delivery_days} weekdays"));
                                    // $date           = date_i18n( 'l', strtotime( "$delivery_date" ) );
                                    // $day            = date_i18n( 'j', strtotime( "$delivery_date" ) );
                                    // $month          = date_i18n( 'M', strtotime( "$delivery_date" ) );
                            ?>
                            
                            <th class="th_custorm">
                                
                                <div class="nbo-delivery-date-wrap nbo-delivery-date-selectorr selectorr-<?php echo $key; ?>" ng-model="deliveryOption[<?php echo $key; ?>]" ng-class="{'active': deliveryOption[<?php echo $key; ?>].active}" ng-click="change_custom_delivery_date(customQty,<?php echo $key; ?>)">
                                    <div class="nbo-delivery-date-inner">
                                        <span class="radio"><i class="fa fa-check" ng-show="deliveryOption[<?php echo $key; ?>].active"></i></span>
                                        <div class="nbo-delivery-date-title" ng-show="deliveryOption[<?php echo $key; ?>].total_cart_price != ''"><?php echo $attr['name']; ?></div>
                                        <div class="nbo-delivery-date2"><span class="day"><?php echo $day; ?></span> <?php echo $month; ?></div>
                                        <div class="nbo-delivery-date-title" ng-show="deliveryOption[<?php echo $key; ?>].total_cart_price != '' && deliveryOption[<?php echo $key; ?>].active">{{deliveryOption[<?php echo $key; ?>].total_cart_price}}</div>
                                        <div class="nbo-delivery-date-title" ng-show="deliveryOption[<?php echo $key; ?>].total_cart_price != '' && !deliveryOption[<?php echo $key; ?>].active">{{deliveryOption[<?php echo $key; ?>].pr_compare}}</div>
                                    </div>
                                </div>
                            </th>
                            <?php endforeach; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="nbo-delivery-custom-quantity" ng-show="valid_form && nbd_fields['<?php echo $delivery_field['id']; ?>'].enable" style="display: none;">
        <span class="nbd-button" ng-hide="custom_quantity" ng-click="custom_quantity = !custom_quantity;" ><?php _e('Add custom quantity', 'web-to-print-online-designer'); ?></span>
        <input type="number" min="1" step="1" ng-show="custom_quantity" ng-model="quantity" ng-keyup="$event.keyCode == 13 && update_turnaround_matrix();" />
        <span class="nbd-button update-custom-quantity" ng-show="custom_quantity" ng-click="update_turnaround_matrix(); custom_quantity = !custom_quantity;">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title><?php _e('update', 'web-to-print-online-designer'); ?></title>
                <path d="M9 16.172l10.594-10.594 1.406 1.406-12 12-5.578-5.578 1.406-1.406z"></path>
            </svg>
        </span>
    </div>
	
     <?php do_action('Show_date_shipping',$attr ); ?>
    <?php
    $html =  ob_get_clean();
    return $html;
 
}