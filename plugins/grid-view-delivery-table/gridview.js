(function ($) {
	var symbol = nbds_frontend.currency_format_symbol,
        decimal = nbds_frontend.currency_format_decimal_sep,
        thousand = nbds_frontend.currency_format_thousand_sep,
        format = nbds_frontend.currency_format;
    $(document).on('change','.switch-vat input:checkbox',function(e){
        if ( $(this).is(":checked") ) {
            console.log('INC');
            //woocommerce price
            var nodelist = $('.woocommerce-Price-amount.amount');
            nodelist.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price * 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            	$(element).next().html(symbol+''+new_price.toFixed(2)+' incl. VAT');
            });
            //delivery price
            var delivery_node = $('.nbo-delivery-total,.nbo-delivery-price-item span');
            delivery_node.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price * 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
            //summary price
            var sum_node = $('.nbo-summary-table tbody tr td:nth-child(2),.nbo-summary-table #nbd-option-total span');
            sum_node.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price * 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
            //footer price
            var sum_node = $('.change-price-foter');
            sum_node.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price * 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
        } else {
            console.log('EXC');
            //woocommerce price
            var nodelist = $('.woocommerce-Price-amount.amount');
            nodelist.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price / 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
            //delivery price
            var delivery_node = $('.nbo-delivery-total,.nbo-delivery-price-item span');
            delivery_node.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price / 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
            //summary price
            var sum_node = $('.nbo-summary-table tbody tr td:nth-child(2),.nbo-summary-table #nbd-option-total span');
            sum_node.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price / 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
            //footer price
            var sum_node = $('.change-price-foter');
            sum_node.each(function(index,element){
            	var price_txt = $(element).text(),
            		price = price_txt.replace(symbol,''),
            		price = parseFloat(price);
            	new_price = price / 1.2;
            	$(element).html(symbol+''+new_price.toFixed(2));
            });
        }
    });
})(jQuery);



