<?php if (!defined('ABSPATH')) { exit; }; ?>
<h2><?php esc_html_e('Select a product', 'web-to-print-online-designer'); ?></h2>
<div class="studio-product-wrap studio-advance">
    <?php foreach( $products as $product ): ?>
    <div class="studio-product" data-id="<?php echo( $product['product_id'] ); ?>" data-template="0" data-collapse="0">
        <div class="studio-product-inner">
            <div class="studio-product-img-wrap">
            <a href="#" target="_blank">
                <img class="product-img-advance" src="<?php echo esc_url( $product['src'] ); ?>" alt="<?php echo( $product['name'] ); ?>" />
                <?php echo( $product['name'] ); ?></a>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php
if( $total > $per_page ): 
    require_once NBDESIGNER_PLUGIN_DIR . 'includes/class.nbdesigner.pagination.php';
    $paging = new Nbdesigner_Pagination();
    $url    = getUrlPageNBD( 'studio' );
    $config = array(
        'current_page'  => isset($page) ? $page : 1,
        'total_record'  => $total,
        'limit'         => $per_page,
        'link_full'     => add_query_arg(array('paged' => '{p}'), $url),
        'link_first'    => $url
    );
    $paging->init($config); 
?>
<div class="tablenav top nbdesigner-pagination-con" id="nbd-pagination">
    <div class="tablenav-pages">
        <div>
            <span class="displaying-num"><?php printf( _n( '%s Product', '%s Products', $total, 'web-to-print-online-designer'), number_format_i18n( $total ) ); ?></span>
            <?php echo $paging->html();  ?>
        </div>
        <div class="spacer"></div>
    </div>
</div>  
<?php endif;