<style>
    .nbd-mode-vista .nbd-warning.active {
        visibility: visible;
        opacity: 1;
        z-index: 11; 
    }
    .nbd-mode-vista .nbd-warning {
        position: absolute;
        top: 60px;
        right: 10px;
        min-width: 100px;
        max-width: 350px;
        border-radius: 2px;
        background-color: transparent;
        visibility: hidden;
        opacity: 0;
        z-index: -1; 
    }
</style>
<div class="nbd-warning" ng-class="( (settings.showWarning.oos && stages[currentStage].states.oos) || (settings.showWarning.ilr && stages[currentStage].states.ilr) ? 'active' : '' )">
<!--    <div class="item main-warning nbd-show">-->
<!--        <i class="nbd-icon-vista nbd-icon-vista-warning warning"></i>-->
<!--        <span class="title-warning">Warning Trouble</span>-->
<!--        <i class="nbd-icon-vista nbd-icon-vista-clear close-warning"></i>-->
<!--    </div>-->
<!---->
	<div class="item main-warning animated animate800" ng-class="settings.showWarning.ilr && stages[currentStage].states.ilr ? 'fadeInDown nbd-show' : 'fadeOutUp'">
        <span class="title-warning"><?php esc_html_e('Image Low Resolution','web-to-print-online-designer'); ?></span>
        <i class="icon-nbd icon-nbd-clear close-popup close-warning" ng-click="settings.showWarning.ilr = false"></i>
    </div>
</div>