<?php if ( ! defined( 'ABSPATH' ) ) { exit;} ?>
<?php ob_start(); ?>
<div class="v-sidebar">
    <div class="main-sidebar">
        <div class="v-tab-contents nbd-shadow">
            <?php include "sidebar/design.php"?>
            <?php include "sidebar/text.php"?>
            <?php include "sidebar/images.php"?>
            <?php include "sidebar/more.php"?>
        </div>
    </div>
</div>
<?php /*nbdesigner advanced*/ echo apply_filters('nbd_hide_sidebars',ob_get_clean());?>