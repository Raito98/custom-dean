<?php
/**
* Plugin Name:       add footer product

* Plugin URI:        https://cmsmart.net
* Version:           1.0.0
* Description:       NBD_advan_custorm
* Requires at least: 5.2
* Requires PHP:      7.1
* Author:            Trung
* Author URI:        https://cmsmart.net
* License:           GPL v2 or later
* License URI:       https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain:       nbdesigner-advanced
* Domain Path:       /languages
*/
add_action('nbd_footer_product','add_footer_product');
function add_footer_product(){
    ?>
    <div class=" product-page-width hidden-sm hidden-xs product-selling-points">
        <div class=" product-selling-point inline-block top">
            <img src="<?php echo plugin_dir_url(__FILE__) .'image/Nextday_icon.png' ?>" class="" width="116" height="131" alt="Competitive Prices">
            <div><strong>Next Day UK
            Delivery </strong>
            available
</div>
        </div><div class=" product-selling-point inline-block top">
            <img src="<?php echo plugin_dir_url(__FILE__) .'image/Plainpacking_icon.png'?>" class="" width="135" height="160" alt="UK Customer Support">
            <div><strong>Plain Label Packaging</strong>
            on all orders
</div>
        </div><div class=" product-selling-point inline-block top">
            <img src=<?php echo plugin_dir_url(__FILE__) .'image/onlinedesigner_green_icon.png'?> class="" width="154" height="167" alt="White Label Packaging">
           <div> <strong>FREE Professional
                Design Software</strong>
            to
create your own artwork</div>
        </div><div class=" product-selling-point inline-block top"> 
            <img src="<?php echo plugin_dir_url(__FILE__) .'image/onlineproof_icon.png'?>"class="" width="153" height="151" alt="Outstanding Quality">
            <div><strong>FREE Online Proofing</strong>
            check your file before
submitting it</div>
        </div>
        <div class=" product-selling-point inline-block top"> 
            <img src="<?php echo plugin_dir_url(__FILE__) .'image/1hrdelivery_icon.png'?>"class="" width="153" height="151" alt="Outstanding Quality">
            <div><strong>1 Hr Delivery
tracking slot</strong>
            for
your peace of mind</div>
        </div>
    </div> 
    <div class="product_information">
        <div class="tab">
            <button class="tablinks active">Description</button>
            <button class="tablinks">Specification</button>
                
        </div>

            <div id="Description" class="tabcontent active">
                <div class="row">
                    <div class="col-md-4 hidden-sm hidden-xs">
                        <img src="<?php echo plugin_dir_url(__FILE__) .'image/Takeawaymenu_pic1.png'?>" alt="image" class="product_info-image">
                    </div>
                    <div class="col-md-8 product_information-content">
                        <h2> Takeaway Menus </h2>
                        <p>Takeaway food is avalable from an array of food outlets, from diners and bakeries to cafes and restaurants.</p>

                        <p>Offer your customers the perfect takeaway menus with our cost effective range</p>


                        </div>
                    </div>
                </div>
                
            </div>

            <div id="Specification" class="tabcontent">
                <div class="row">
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <img src="<?php echo plugin_dir_url(__FILE__) .'image/Take-away-Menu_4_800X800.jpg'?>" alt="image" class="product_info-image">
                        </div>
                        <div class="col-md-8 product_information-content">
                            <h2> Technical Specifications for Takeaway Menu Printing </h2>
                            <ul>
                            <li>Artwork templates are available for download to assist with setting up your files. These include the panel widths required, too.</li>
                            <li>Before submitting Takeaway Menu artwork, we recommend you print a mock up to check the artwork is positioned correctly.</li>
                            <li>When creating any leaflet, please supply your artwork as a 2 page, double sided PDF. For example, on an A4 folded to A5, page 1 should display pages 4 &amp; 1 of the leaflet and page 2 should display pages 2 &amp; 3,&nbsp;<a href="https://help.tradeprint.co.uk/hc/en-us/articles/360002800273-Folded-Leaflet-artwork-setup">as shown in this illustration</a>.&nbsp;</li>
                            <li>When creating artwork with a landscape fold, please submit the front and back pages on the same page, with the front page orientation correct and the back page rotated 180°. The inside pages should be on page 2 and rotated 180° so that the orientation is correct on the final print. You can check the orientation is correct by printing a mock up before submitting your artwork.</li>
                            <li>Include 3mm bleed on all sides</li>
                            <li>Supply as a CMYK PDF</li>
                            <li>Outline or embed fonts</li>
                            <li>300dpi resolution</li>
                        </ul>

                        <p>For more guidance on how to set up artwork for Takeaway Menu, plus answer other product questions, visit our&nbsp;<a href="https://help.tradeprint.co.uk/hc/en-us/sections/360000484913-Folded-Leaflets">folded leaflets Help Centre&nbsp;pages</a>.&nbsp;</p>

                    
                </div>
            </div>
        </div>
    </div>
        <script type="text/javascript">
        var buttons = document.getElementsByClassName('tablinks');
            var contents = document.getElementsByClassName('tabcontent');
            function showContent(id){
                for (var i = 0; i < contents.length; i++) {
                    contents[i].style.display = 'none';
                }
                var content = document.getElementById(id);
                content.style.display = 'block';
            }
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].addEventListener("click", function(){
                    var id = this.textContent;
                    for (var i = 0; i < buttons.length; i++) {
                        buttons[i].classList.remove("active");
                    }
                    this.className += " active";
                    showContent(id);
                });
            }
            showContent('Description');
        </script>
    <?php

}
add_action('add_tab_panel','add_tab');
function add_tab(){
    ?>
    <div class="product-page_left">
        <div class="artwork-guide-accordion">
            <strong class="artwork-guide-heading-accordion">Artwork Guide <i class="fa pull-right fa-chevron-right" aria-hidden="true"> </i></strong>
            <div class="artwork-guide-accordion-content">
                 <p>
                    <?php 
                        global $product;
                        $pid = $product->get_id();
                        $_rony_product_infor = get_post_meta($pid, '_rony_product_infor');
                        echo $_rony_product_infor[0];
                    ?>
                </p>
     
            </div>
        </div>
        
        <div class="artwork-template-accordion">
            <strong class="artwork-template-heading-accordion">Artwork Templates <i class="fa pull-right fa-chevron-right" aria-hidden="true"> </i></strong>
        
            <?php do_action('nbd_request_design_after_product_image');?>
        </div>
    </div>

    
    <script type="text/javascript">
        jQuery(".artwork-template-accordion").bind("click", function(){
       {
          if((jQuery(".nbdg-guideline-wrap").css("display") == 'block')){
              jQuery(".nbdg-guideline-wrap").css("display","none");
            

          }else {
            jQuery(".nbdg-guideline-wrap").css("display","block");
            
          }
      }
      
        
          
    });
        jQuery(".artwork-guide-accordion").bind("click", function(){
       {
          if((jQuery(".artwork-guide-accordion-content").css("display") == 'block')){
              jQuery(".artwork-guide-accordion-content").css("display","none");
            

          }else {
            jQuery(".artwork-guide-accordion-content").css("display","block");
            
          }
      }
      });
    </script>
    <?php
}
add_action('nbd_add_enable_check_spine', 'form_text');
function form_text($post_id)
{
    $post_id = $_GET['post'];
    $_rony_product_infor = get_post_meta($post_id, '_rony_product_infor', true) ? get_post_meta($post_id, '_rony_product_infor', true) : '';
    ?>
    <span class="nbo-option-val">
        <label>Product Info</label>
        <span class="nbo-option-val">
            <input style="width: 100%;" type="text" value="<?php echo $_rony_product_infor; ?>" name="_nbdc[_rony_product_infor]" id="_enable_text_info" />
        </span>
    </span>
    

<?php
}
add_action('nbo_save_options', 'woo_add_custom_general_fields_savedd');
function woo_add_custom_general_fields_savedd($post_id)
{
    $_enable_text_info = $_POST['_nbdc'];
    foreach ($_enable_text_info as $key => $value) {
        update_post_meta($post_id, $key, $value);
    }
}


add_action('box_up_fix','add_box_up');
function add_box_up($attr)
{
global $product;
 
    
            $delivery_days  = absint( $attr['delivery'] );
            $delivery_date  = date('Y-m-d', strtotime("+{$delivery_days} weekdays"));
            $date           = date_i18n( 'l', strtotime( "$delivery_date" ) );
            $day            = date_i18n( 'j', strtotime( "$delivery_date" ) );
            $month          = date_i18n( 'M', strtotime( "$delivery_date" ) );
 
    ?>
    <div class="product-fixed-add-container ">
        <div class="container">
            <div class="row">
                <div class="col-md-6 product-fixed">
                    <div>  
                        <div class="footer_date"><?php echo  $date.' '.$day.' '.$month ?></div>
                        <span class="f-16 mr-2 bold">Total: <strong class="f-18 txt-heavy change-price-foter"><?php echo wc_price ($product->get_price());?></strong></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div  class="text-right">
                        <button class="btn btn-orange btn-block btn-add-to-basket small single_add_to_cart_button" show() id="btn-add-to-basket">Add to Basket</button>
                        <?php echo do_shortcode( '[nbdesigner_button]' ); ?>
                    </div>
                </div>
            </div>
          
                    
    </div>
                
        
    <script type="text/javascript">
            
            jQuery("#btn-add-to-basket").click(function(){
                jQuery(".single_add_to_cart_button").click();
            })
            
        </script>
        <script type="text/javascript">
      
      jQuery('.upload_degin').click(function(){
        jQuery('.nbd-action-wrap .start-design').hide();
        jQuery('.single_add_to_cart_button').hide();
        jQuery('.nbd-action-wrap #startUpload').show();
      });
      jQuery('.start_degin').click(function(){
        jQuery('.nbd-action-wrap .upload-design').hide();
        jQuery('.single_add_to_cart_button').hide();
        jQuery('.nbd-action-wrap #startDesign').show();
      });
      jQuery('.add_cart').click(function(){
        jQuery('.nbd-action-wrap .upload-design').hide();
        // jQuery('.single_add_to_cart_button').hide();
        jQuery('.nbd-action-wrap .start-design').hide();
        jQuery('.single_add_to_cart_button').show();
      });
      </script>
    <?php
}

add_action('Show_date_shipping','show_date');
function show_date($attr){
    ?>
    <?php 
    
            $delivery_days  = absint( $attr['delivery'] );
            $delivery_date  = date('Y-m-d', strtotime("+{$delivery_days} weekdays"));
            $date           = date_i18n( 'l', strtotime( "$delivery_date" ) );
            $day            = date_i18n( 'j', strtotime( "$delivery_date" ) );
            $month          = date_i18n( 'M', strtotime( "$delivery_date" ) );
    ?>
    <div class="text-center del_service-date fastest ">
        <div class="orderBy">
            <span id="countdown"></span>
        </div>
        <div class="production-start-date"><span><?php echo  $date.' '.$day.' '.$month ?></span></div>
    </div>
     <script type="text/javascript">
        let tomorrow =  new Date();
        var countDownDate = new Date((tomorrow.getMonth()+1) +' '+ (tomorrow.getDate() + 1 ) + ','+ tomorrow.getFullYear()).getTime();
        
        // Update the count down every 1 second
        var x = setInterval(function() {
        
          // Get today's date and time
          var now = new Date().getTime();
            
          // Find the distance between now and the count down date
          var distance = countDownDate - now;
          // Time calculations for days, hours, minutes and seconds
           const second = 1000,
                minute = second * 60,
                hour = minute * 60,
                day = hour * 24;
            var hours = Math.floor((distance % (day)) / (hour));
            var minutes = Math.floor((distance % (hour)) / (minute));
            var seconds = Math.floor((distance % (minute)) / second);
        
          // Output the result in an element with id="countdown"
          document.getElementById("countdown").innerHTML = "Order within " +  hours + " hr "
          + minutes + " mins " + seconds + " secs " + "to receive by";
            
          // If the count down is over, write some text 
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "EXPIRED";
          }
        }, 1000);
            
            
            
        </script>
    <?php
}
define('urlimage',plugin_dir_url(__FILE__));
add_filter('button_artwork','change_button',10,1);
function change_button($html){
    ?>
<div>
    <div class="Icon-img-container">
        <div class="Icon-inner-container upload_degin_buttom">
            <div id="circle">
                <img class="circle-image" src="<?php echo urlimage.'/image/Upload_icon.png' ?>" alt="upload_artwork">
            </div>
            <div id="card" class="upload_degin">
                <div class="card-header"> Upload Artwork</div>
                <div class="card-content">If you have ‘Print Ready’ artwork you can upload it here</div>
            </div>
        </div>
        <div class="Icon-inner-container start_degin_buttom">
            <div id="circle">
                <img class="circle-image" src=" <?php echo urlimage. '/image/onlinedesigner_icon.png' ?>" alt="design_online">
            </div>
          <div id="card" class="start_degin">
                <div class="card-header"> Design Online</div>
                <div class="card-content">If you don’t have a design,
you can create one here
using our simple design tool
</div>
            </div>
        </div>
        <div class="Icon-inner-container add_cart_buttom">
            <div id="circle">
                <img class="circle-image" src="<?php echo urlimage. '/image/Designservice_icon.png'?>" alt="add_to_basket ">
            </div>
            <div id="card" class="add_cart">
                <div class="card-header">Use our Professional
Design Service</div>
                <div class="card-content">Let our in-house design team
create your artwork for you</div>
            </div>
        </div>
    </div>
  </div>
<?php echo do_shortcode( '[nbdesigner_button]' ); ?>

    <style type="text/css">
        .Icon-img-container {
            display: flex;
            margin-top: 10px;
            margin-bottom: 10px;
            justify-content: space-between;
        }
        .Icon-inner-container {
            position: relative;
            display: flex;
            justify-content: center;
            border: 1px solid #E9E9E9;
        }
        #circle {
            width: 50px;
            height: 50px;
            border-radius: 50px;
            opacity: 1;
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            z-index: 1;
            margin-top: 5px;
        }

        .card-success{
            background-color:#ccc;
        }
        .card-header {
            padding-top: 40px;
            text-align: center;
            font: normal normal bold 14px/18px Montserrat;
            color: #000;
            font-size:
            opacity: 1;
            font-size: 14px;
            letter-spacing: 0;
            font-weight: 700;
        }
        .card-content {
            margin: 5%;
            text-align: center;
            font-size: 14px;
            color: #666;
            opacity: 1;
            letter-spacing: 0;
            line-height: 1.2;
        }
        #card {
            
            border-width: thin;
            border-radius: 2px;
            opacity: 1;
            height: 150px;
            max-height: fit-content;
            width: 180px;
            margin-top: 25px;
        }
        
    </style>
	<script type="text/javascript">
      
      jQuery('.upload_degin').click(function(){
        jQuery('.upload_degin_buttom').addClass('card-success');
        jQuery('.start_degin').removeClass('card-success');
        jQuery('.single_add_to_cart_button').removeClass('card-success');
        jQuery('.add_cart_buttom').removeClass('card-success');
        jQuery('.start_degin_buttom').removeClass('card-success');
        jQuery('#startDesign').hide();
        jQuery('.single_add_to_cart_button').hide();
        jQuery('.nbd-action-wrap #startUpload').css("display","block");
      });
      jQuery('.start_degin').click(function(){
        jQuery('.start_degin_buttom').addClass('card-success');
        jQuery('.add_cart_buttom').removeClass('card-success');
        jQuery('.upload_degin_buttom').removeClass('card-success');
        jQuery('#startUpload').hide();
        jQuery('.single_add_to_cart_button').hide();
        jQuery('.nbd-action-wrap #startDesign').css("display","block");
      });
      jQuery('.add_cart').click(function(){
        jQuery('.add_cart_buttom').addClass('card-success');
        jQuery('.start_degin_buttom').removeClass('card-success');
        jQuery('.upload_degin_buttom').removeClass('card-success');
        jQuery('#startUpload').hide();
        jQuery('.single_add_to_cart_button').hide();
        jQuery('#startDesign').hide()
        jQuery('.single_add_to_cart_button').show();
      });
      </script>
    <?php
}

add_action('delivery_included','delivery');
function delivery(){
    ?>
    <div class="delivery-strapline">
       
        <div class="icon-ship">
        <img src="<?php echo urlimage.'image/car.svg' ?>"/>
        </div>
            <span class="inline-block mid">Mainland UK Delivery Included</span>

    </div>
    <style type="text/css">
    .woocommerce-product-gallery__wrapper{
        position: relative;
    }
        .inline-block.mid {
            vertical-align: middle;
            color:#fff;
        }
        .delivery-strapline {
            border: 1px ;
            padding: 8px 2px 8px;
            margin: 0;
            font-weight: 700;

            display: flex;
            align-items: baseline;
            left:21px;
            background:#017cc0;
            position: absolute;
            bottom: 0;
            width: 94%;
            z-index: 9;
        }
        .icon-ship{
            margin-right: 15px;
            margin-left: 15px;
        }
        .inline-block {
            display: inline-block;
            float: none;
        }
        .icon-ship img{
            width:35px;
            height:35px;
            
           
        }
        
    </style>
    <?php
}

add_filter('clear_select','clean_select');
function clean_select(){
    return '';
}
register_activation_hook( __FILE__, 'add_product_thevu' );
function add_product_thevu(){
    $products = array(
        array(499, 4, 10, 'simple', 'Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-26 11:43:52', '2020-03-09 12:07:03', 1, 0),
        array(508, 4, 10, 'simple', 'Letterheads and Comp Slips', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-06-05 16:23:18', '2017-06-20 13:34:15', 1, 0),
        array(509, 4, 10, 'simple', 'Carbonless Invoice Pads', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-06 16:27:15', '2017-06-20 13:35:12', 1, 0),
        array(510, 4, 10, 'simple', 'test_canvas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-05-06 20:36:30', '2017-07-17 17:04:06', 1, 0),
        array(517, 4, 10, 'simple', 'test product ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-10-27 18:53:04', '2018-01-12 13:09:59', 1, 0),
        array(519, 4, 10, 'simple', 'Folded Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-10 18:17:50', '2018-01-16 23:43:35', 1, 0),
        array(523, 4, 10, 'simple', 'A4 Posters', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-11 19:48:24', '2020-03-17 16:51:03', 1, 0),
        array(524, 4, 10, 'simple', 'Pull up Roller Banners', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-11 22:07:12', '2018-05-14 13:13:34', 1, 0),
        array(525, 4, 10, 'simple', 'A3 Posters ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-11 23:41:02', '2020-03-17 16:52:02', 1, 0),
        array(526, 4, 10, 'simple', 'A2 Posters ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-11 23:48:25', '2020-03-17 16:52:22', 1, 0),
        array(527, 4, 10, 'simple', 'A1 Posters ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-12 18:37:20', '2020-03-17 16:52:39', 1, 0),
        array(528, 4, 10, 'simple', 'A0 Posters ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-12 18:54:52', '2020-03-17 16:55:49', 1, 0),
        array(529, 4, 10, 'simple', 'B2 Posters ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-12 19:12:35', '2018-02-20 18:12:14', 1, 0),
        array(530, 4, 10, 'simple', 'B1 Posters ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-12 19:21:12', '2018-02-20 18:11:30', 1, 0),
        array(531, 4, 10, 'simple', '20 x 30 Posters', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-16 23:12:15', '2020-03-06 15:41:50', 1, 0),
        array(532, 4, 10, 'simple', '30 x 40 Posters', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-16 23:15:09', '2020-02-14 14:09:58', 1, 0),
        array(533, 4, 10, 'simple', '60 x 40 Posters', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-16 23:19:10', '2020-03-17 17:09:48', 1, 0),
        array(534, 4, 10, 'simple', 'A5 Folded Menus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 00:00:44', '2018-04-11 23:27:28', 1, 0),
        array(535, 4, 10, 'simple', 'A3 Folded Menus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 00:26:46', '2018-04-11 23:19:11', 1, 0),
        array(536, 4, 10, 'simple', 'Extended A4 Menus array(4 panels)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 00:33:43', '2018-04-11 23:04:13', 1, 0),
        array(537, 4, 10, 'simple', 'A4 Folded Menus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 00:33:55', '2018-04-11 23:26:46', 1, 0),
        array(538, 4, 10, 'simple', '148mm Square Folded Menus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 00:51:44', '2018-04-11 23:06:52', 1, 0),
        array(539, 4, 10, 'simple', '210mm Square Folded Menus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 00:51:52', '2018-04-11 23:18:13', 1, 0),
        array(540, 4, 10, 'simple', 'A4 Slim Folded Menus ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-26 01:15:11', '2018-04-17 22:39:59', 1, 0),
        array(541, 4, 10, 'simple', 'A4 Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 21:16:05', '2020-03-09 12:05:16', 1, 0),
        array(542, 4, 10, 'simple', 'A7 Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 21:16:12', '2020-03-09 13:12:19', 1, 0),
        array(543, 4, 10, 'simple', 'A5 Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 21:16:20', '2020-03-06 16:58:26', 1, 0),
        array(544, 4, 10, 'simple', 'BLANK FOR FLYERS ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 21:47:32', '2018-01-31 21:49:09', 1, 0),
        array(545, 4, 10, 'simple', 'Square Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 21:47:55', '2018-04-17 19:20:46', 1, 0),
        array(546, 4, 10, 'simple', 'DL Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 21:57:55', '2020-03-09 14:48:47', 1, 0),
        array(547, 4, 10, 'simple', 'BLANK WITH FOLDING OPTIONS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2018-01-31 22:17:25', '2018-01-31 22:18:16', 0, 0),
        array(548, 4, 10, 'simple', 'Economy Business Cards array(Cheapest)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-01-31 22:18:37', '2020-01-28 16:52:28', 1, 0),
        array(550, 4, 10, 'simple', 'Loyalty Cards ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 18:57:10', '2018-05-15 19:36:19', 1, 0),
        array(551, 4, 10, 'simple', 'Gloss Laminated Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 18:57:40', '2018-05-22 20:05:30', 1, 0),
        array(552, 4, 10, 'simple', 'Matt Laminated Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 18:58:16', '2018-05-22 20:03:08', 1, 0),
        array(553, 4, 10, 'simple', 'Uncoated Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 19:15:59', '2018-05-15 19:34:45', 1, 0),
        array(554, 4, 10, 'simple', 'Rounded Corner Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 19:22:53', '2018-05-22 20:03:57', 1, 0),
        array(555, 4, 10, 'simple', 'Textured Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 19:23:12', '2018-05-11 20:51:44', 1, 0),
        array(556, 4, 10, 'simple', 'Square Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 19:23:34', '2018-05-15 18:29:01', 1, 0),
        array(557, 4, 10, 'simple', 'Mini Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 19:23:57', '2018-05-15 19:33:17', 1, 0),
        array(558, 4, 10, 'simple', 'Metallic Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 19:49:00', '2019-05-19 14:08:20', 1, 0),
        array(559, 4, 10, 'simple', 'A4 Slim Laminated Folding Flyers & Leaflets array(297mm x 105mm)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 20:55:44', '2018-04-17 21:49:03', 1, 0),
        array(568, 4, 10, 'simple', '210mm Square Folded Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 21:04:02', '2018-04-11 23:16:57', 1, 0),
        array(569, 4, 10, 'simple', '148mm Square Folded Flyers & Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 21:05:39', '2020-02-20 11:27:39', 1, 0),
        array(570, 4, 10, 'simple', 'A4 Folded Flyers & Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 21:10:54', '2020-04-15 08:45:00', 1, 0),
        array(571, 4, 10, 'simple', 'Extended A4 Laminated Folding Flyers & Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 21:13:29', '2018-04-17 21:48:41', 1, 0),
        array(572, 4, 10, 'simple', 'A3 Folded Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 21:16:25', '2020-04-15 08:54:46', 1, 0),
        array(573, 4, 10, 'simple', 'A5 Folded Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 21:17:46', '2020-04-14 12:09:05', 1, 0),
        array(575, 4, 10, 'simple', 'A5 Stapled Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-02 23:01:47', '2018-05-16 19:03:53', 1, 0),
        array(576, 4, 10, 'simple', 'A4 Laminated Short Folding Flyers & Leaflets array(To Cust Spec)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-08 00:01:25', '2018-04-17 21:29:51', 1, 0),
        array(577, 4, 10, 'simple', '4pp DL Laminated Folding Flyers & Leaflets array(99mm x 210mm)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-08 00:01:31', '2018-04-17 21:48:14', 1, 0),
        array(578, 4, 10, 'simple', 'A4 Stapled Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:14:09', '2018-04-17 22:59:33', 1, 0),
        array(579, 4, 10, 'simple', 'A6 Stapled Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:14:16', '2018-04-17 22:54:18', 1, 0),
        array(580, 4, 10, 'simple', 'DL Stapled Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:14:23', '2018-04-17 23:01:15', 1, 0),
        array(581, 4, 10, 'simple', '148mm Square Stapled Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:14:33', '2018-04-17 23:02:16', 1, 0),
        array(583, 4, 10, 'simple', 'A5 Perfect Bound Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:22:41', '2018-04-12 22:45:13', 1, 0),
        array(589, 4, 10, 'simple', 'A6 Perfect Bound Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:38:50', '2018-04-12 22:43:31', 1, 0),
        array(590, 4, 10, 'simple', 'DL Perfect Bound Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:39:01', '2018-04-12 22:48:41', 1, 0),
        array(591, 4, 10, 'simple', '210mm Square Perfect Bound Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:39:37', '2018-04-12 22:52:05', 1, 0),
        array(592, 4, 10, 'simple', '148mm Square Perfect Bound Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:39:46', '2018-04-12 22:50:35', 1, 0),
        array(596, 4, 10, 'simple', 'A4 Perfect Bound Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-09 02:47:57', '2018-04-12 22:47:10', 1, 0),
        array(597, 4, 10, 'simple', '210mm Square Stapled Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 00:14:40', '2018-04-17 23:03:14', 1, 0),
        array(598, 4, 10, 'simple', 'A5 Wiro Bound Booklets & Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 01:21:09', '2018-04-12 22:55:21', 1, 0),
        array(599, 4, 10, 'simple', 'A4 Wiro Bound Booklets & Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 01:21:27', '2018-04-12 22:56:45', 1, 0),
        array(600, 4, 10, 'simple', 'A6 Wiro Bound Booklets & Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 01:21:44', '2018-04-12 22:54:01', 1, 0),
        array(601, 4, 10, 'simple', 'DL Wiro Bound Booklets & Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 01:22:02', '2018-04-12 22:58:12', 1, 0),
        array(602, 4, 10, 'simple', '148mm Square Wiro Bound Booklets & Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 01:22:22', '2018-04-12 23:07:40', 1, 0),
        array(603, 4, 10, 'simple', '210mm Square Wiro Bound Booklets & Brochures', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-14 01:38:30', '2018-04-17 22:58:47', 1, 0),
        array(604, 4, 10, 'simple', 'Appointment Cards ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-21 21:56:53', '2018-05-22 20:06:47', 1, 0),
        array(605, 4, 10, 'simple', 'Wide Roller Banners', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-21 22:03:22', '2018-05-14 13:15:27', 1, 0),
        array(606, 4, 10, 'simple', 'Premium Roller Banners ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-21 22:42:23', '2018-05-14 13:15:51', 1, 0),
        array(607, 4, 10, 'simple', 'Double Sided Roller Banners', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-21 22:42:31', '2018-05-14 13:16:15', 1, 0),
        array(608, 4, 10, 'simple', 'Desktop Roller Banners', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-21 23:15:20', '2018-04-04 22:05:44', 1, 0),
        array(609, 4, 10, 'simple', 'Folded appointment cards ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-02-21 23:46:09', '2018-05-15 19:32:07', 1, 0),
        array(610, 4, 10, 'simple', 'PVC Banners', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-03-08 23:04:11', '2018-04-13 12:54:15', 1, 0),
        array(612, 4, 10, 'simple', 'test 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2018-03-20 21:38:53', '2018-03-20 21:40:05', 0, 0),
        array(615, 4, 10, 'simple', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-03-23 19:14:57', '2018-03-23 19:23:48', 1, 0),
        array(616, 4, 10, 'simple', 'Folded Business ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-03-23 22:38:45', '2018-05-22 20:23:59', 1, 0),
        array(617, 4, 10, 'simple', 'A3 Folded Flyers & Leaflets test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-13 12:55:41', '2020-04-14 07:48:08', 1, 0),
        array(618, 4, 10, 'simple', 'A7 Laminated Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 17:29:04', '2020-02-14 16:23:12', 1, 0),
        array(619, 4, 10, 'simple', 'A6 Laminated Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 18:51:21', '2018-04-17 20:25:13', 1, 0),
        array(620, 4, 10, 'simple', 'A5 Laminated Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 19:11:54', '2018-04-17 20:25:51', 1, 0),
        array(621, 4, 10, 'simple', 'Square Laminated Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 19:16:48', '2020-02-18 15:19:01', 1, 0),
        array(622, 4, 10, 'simple', 'DL Laminated Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 20:10:18', '2018-04-17 20:22:04', 1, 0),
        array(623, 4, 10, 'simple', 'A4 Laminated Flyers and Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 20:14:43', '2018-04-17 20:22:35', 1, 0),
        array(624, 4, 10, 'simple', 'A5 Laminated Folding Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 20:28:21', '2018-04-17 22:21:21', 1, 0),
        array(625, 4, 10, 'simple', '148mm Square Laminated Folding Flyers & Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 20:44:54', '2018-04-17 20:51:43', 1, 0),
        array(626, 4, 10, 'simple', 'A4 Laminated Folding Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 20:52:06', '2018-04-17 21:06:38', 1, 0),
        array(627, 4, 10, 'simple', '210mm Square Laminated Folding Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 21:07:44', '2018-04-17 21:47:47', 1, 0),
        array(628, 4, 10, 'simple', 'A3 Laminated Folding Flyers & Leaflets ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 21:39:56', '2020-04-14 07:48:51', 1, 0),
        array(629, 4, 10, 'simple', 'Extended A4 Folded Flyers & Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 21:52:29', '2018-04-17 22:43:03', 1, 0),
        array(630, 4, 10, 'simple', '4pp DL Folded Flyers & Leaflets array(99mm x 210mm)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 22:27:00', '2018-04-17 22:30:23', 1, 0),
        array(631, 4, 10, 'simple', 'A4 Short Folded Flyers & Leaflets array(To Cust Spec)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 22:32:48', '2018-04-17 22:36:48', 1, 0),
        array(632, 4, 10, 'simple', 'A4 Slim Folded Flyers & Leaflets', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-17 22:37:37', '2018-04-17 22:39:26', 1, 0),
        array(633, 4, 10, 'simple', 'A5 Order Of Service ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-04-18 18:50:25', '2018-04-18 19:34:53', 0, 0),
        array(641, 4, 10, 'simple', 'TEST Standard Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-05-03 01:45:51', '2018-05-08 20:28:39', 1, 0),
        array(642, 4, 10, 'simple', 'TEST - % ON TURNAROUND - Standard Business Cards', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-05-03 17:12:01', '2018-05-03 18:21:35', 1, 0),
        array(643, 4, 10, 'simple', 'test1 Sample', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-05-03 17:39:04', '2019-05-15 12:22:57', 1, 0),
        array(645, 4, 10, 'simple', 'Folded appointment cards new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-05-15 19:18:42', '2018-05-22 21:58:55', 1, 0),
        array(655, 4, 10, 'simple', 'Bookmark 148 x 52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-06-28 20:46:17', '2020-02-14 16:38:06', 1, 0),
        array(657, 4, 10, 'simple', 'Double Royal   25 x 40 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-02-14 14:35:03', '2020-03-06 10:43:48', 0, 0)
    );
    foreach($products as $pro){
        $post_id = wp_insert_post( array(
            'post_title' => $pro[4],
            'post_type' => 'product',
            'post_status' => 'publish',
            'post_content' => '',
        ));
        if($post_id){
            update_post_meta( $post_id, '_regular_price', '0');
        	update_post_meta( $post_id, '_price', '0');
            $product = wc_get_product( $post_id );
            $product->set_price( 0 );
            $product->save();
        }
        else{
            print_r($pro);
        }
        
    }
}